import csv
import requests
from bs4 import BeautifulSoup


def page_parse(soup):
    """
    Парсинг одной страницы, "сырой" результат
    """
    page_data = list()
    data = list()
    table = soup.find('table')
    row = table.find_all('tr')
    for col in row:
        inf = col.find_all('td')
        for one in inf:
            try:
                links = one.find_all('a')
            except Exception as e:
                pass
            else:
                for href in links:
                    if 'houses' in href.get('href'):
                        data.append(href.get('href')[13:])
                    elif 'company' in href.get('href'):
                        data.append(href.get('href')[14:])
            data.append(one.text)
        data = []
        page_data.append(data)
    print('ParsPred')
    return page_data


def soup_parse(page_data):
    """
    Дополнительный фильтр пропаршенных данных страницы
    """
    a = 0 
    del page_data[-1]
    for i in page_data.copy():
        del page_data[a][0]
        a+=1
    pret_data = page_data
    print('ParsPost')
    return pret_data


def gen_url():
    """
    Генератор URL'ов
    BAD: 2**16 - некое заведомо запредельное значение
    """
    base_url = 'http://gosjkh.ru/houses/yaroslavskaya-oblast/yaroslavl?page=%s'
    for url in [base_url % (i+1) for i in range(2**16)]: #  once it changes, it's bad :(
        print(url)
        yield url


def save_txt(full_data):
    """
    Сохранение в формате .txt
    """
    sub_nm = 'Ярославская область' #  soup.find_all(itemprop="title")[1].text   #  once it changes, it's bad :(
    mun_id = 'yaroslavl'           #                                            #  once it changes, it's bad :(
    mun_nm = 'Ярославль'           #  soup.find_all(itemprop="title")[2].text   #  once it changes, it's bad :(
    header = ['ID дома', 'Адрес дома', 'ID управляющей организации', 'Наименование управляющей организации', 'Год постройки', 'Площадь', 'Количество жителей']   
    with open('ParYar.txt', 'w') as outf:
        outf.write(sub_nm)
        outf.write('\n')    
        outf.write(mun_id)
        outf.write('\n')
        outf.write(mun_nm)
        outf.write('\n')
        outf.write(header)
        outf.write('\n')
        for data in full_data:
            for one in data:
                outf.write(one)
                outf.write(', ')
            outf.write('\n')
    print('TXT')


def save_csv(full_data):
    """
    Сохранение в формате .csv
    """
    sub_nm = ['Ярославская область'] #  soup.find_all(itemprop="title")[1].text   #  once it changes, it's bad :(
    mun_id = ['yaroslavl' ]          #                                            #  once it changes, it's bad :(
    mun_nm = ['Ярославль' ]          #  soup.find_all(itemprop="title")[2].text   #  once it changes, it's bad :(
    header = ['ID дома', 'Адрес дома', 'ID управляющей организации', 'Наименование управляющей организации', 'Год постройки', 'Площадь', 'Количество жителей']   
    with open('ParYar.csv', "w", newline='') as outf:
        writer = csv.writer(outf, delimiter=',')
        writer.writerow(sub_nm)
        writer.writerow(mun_id)
        writer.writerow(mun_nm) 
        writer.writerow(header)
        for line in full_data:
            writer.writerow(line)
    print('CSV')


def req_time(url):
    """
    Решение проблемы отсутствия response от сайта рекурсивным вызовом.
    BAD: возможен 429 error --> sleep\\proxy
    """
    try:
        r = requests.get(url, timeout=1)
    except:
        print('buka')
        r = req_time(url)
    return r


def main():
    """
    Ход:
        > Запршиваем очередную страницу целиком
            > Если не пришло, рекурсивно повторяем запрос
        > Парсим страницу
            > Если парс текущей страницы пустой, заканчиваем
        > Фильтруем парс
        > Загоняем данные в список (BAD: слишком много памяти)
        > После всех парсов сохраняем список в файл
    """
    full_data = list()
    for url in gen_url():
        r = req_time(url) 
        print('ReqGot')
        soup = BeautifulSoup(r.text, 'html.parser')     
        page_data = page_parse(soup)
        if page_data == [[]]:
            break
            print('br')
        pret_data = soup_parse(page_data)
        for home in pret_data: 
            full_data.append(home)
    save_txt(full_data)
    save_csv(full_data)


if __name__ == '__main__':
    main()
